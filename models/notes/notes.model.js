import mongoose, { Schema } from "mongoose";
import { isImgUrl } from "../../helpers/CheckMethods";
import autoIncrement from 'mongoose-auto-increment';
const notesSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    user: {
        type: Number,
        ref: 'user',
    },
    description:{
        type:String,
        required:true
    },
    product: {
        type: Number,
        ref: 'product'
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true });

notesSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
notesSchema.plugin(autoIncrement.plugin, { model: 'notes', startAt: 1 });

export default mongoose.model('notes', notesSchema);