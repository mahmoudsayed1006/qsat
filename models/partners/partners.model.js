import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';


const PartnersSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true,
    },
    cost: {
        type: Number,
        required: true,
    },
    percentage:{
        type: Number,
        required:true
    },
    profit:{
        type: Number,
    },
    status:{
        type:Boolean,
        default:false
    },
    deleted:{
        type:Boolean,
        default:false
    }
});

PartnersSchema.set('toJSON', {
    transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        delete ret.deleted;
    }
});


PartnersSchema.plugin(autoIncrement.plugin, { model: 'partners', startAt: 1 });

export default mongoose.model('partners', PartnersSchema);
