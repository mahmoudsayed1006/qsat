import mongoose, { Schema } from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const PremiumsSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    client: {
        type: Number,
        ref: 'user',
    },
    product:{
        type: Number,
        ref: 'product',
    },
    salesMan: {
        type: Number,
        ref: 'user',
        required:true
    },
    branch: {
        type: Number,
        ref: 'branch',
        //required:true
    },
    status: {
        type: String,
        enum: ['PENDING','FINISHED'],
        default: 'PENDING'
    },
    totalMonth:{
        type: Number,
        required:true
    },
    costRemaining:{
        type: Number,
        required:true
    },
    monthRemaining:{
        type: Number,
        required:true
    },
    paidMonth:{
        type: Number,
        required:true
    },
    costPerMonth:{
        type: Number,
    },
    firstPaid:{
        type: Number,
        required:true
    },
    paidCost:{
        type: Number,
        required:true
    },
    paidDate:{
        type: Date,
        required:true
    },
    notes:{
        type: String
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true });

PremiumsSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
PremiumsSchema.plugin(autoIncrement.plugin, { model: 'premiums', startAt: 1 });

export default mongoose.model('premiums', PremiumsSchema);