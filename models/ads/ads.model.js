import mongoose, { Schema } from "mongoose";
import { isImgUrl } from "../../helpers/CheckMethods";
import autoIncrement from 'mongoose-auto-increment';
const AdsSchema=new Schema({
    _id: {
        type: Number,
        required: true
    },
    img: [{
        type: String,
        required: true,
        
    }],
    description:{
        type:String,
        required:true
    },
    top:{
        type:Boolean,
        default:false
    },
    visible: {
        type: Boolean,
        default: true
    },
    deleted:{
        type:Boolean,
        default:false
    },

},{ timestamps: true });
AdsSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
AdsSchema.plugin(autoIncrement.plugin, { model: 'ads', startAt: 1 });

export default mongoose.model('ads', AdsSchema);