import mongoose, { Schema } from "mongoose";
import { isImgUrl } from "../../helpers/CheckMethods";
import autoIncrement from 'mongoose-auto-increment';
const ProductSchema=new Schema({
    _id: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    cashPrice: {
        type: Number,
    },
    installmentPrice: {
        type: Number,
    },
    salesRatio:{
        type: Number,
    },
    quantity:{
        type:Number,
        required:true
    },
    category: {
        type: [Number],
        ref: 'category'
    },
    company:{
        type:String,
        required:true
    },
    img: [{
        type: String,
        required: true,
        
    }],
    properties: [new Schema({
        attr: {
            type: String,
        },
        value: {
            type: String
        },
       
        
    })],
    description:{
        type:String,
        required:true
    },
    hasOffer: {
        type: Boolean,
        default: false
    },
    offer: {
        type: Number,
        default: 0
    },
    offerDescription:{
        type:String,
    },
    visible: {
        type: Boolean,
        default: true
    },
    comments:{
        type:[Number],
        ref: 'comment'
    },
    top:{
        type:Boolean,
        default:false
    },
    deleted:{
        type:Boolean,
        default:false
    },

},{ timestamps: true });
ProductSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete  ret.numerator;
        delete  ret.denominator;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
ProductSchema.plugin(autoIncrement.plugin, { model: 'product', startAt: 1 });

export default mongoose.model('product', ProductSchema);