import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
import { isImgUrl } from "../../helpers/CheckMethods";
const CategorySchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    categoryname: {
        type: String,
        trim: true,
        required: true,
    },
    arabicname: {
        type: String,
        trim: true,
        required: true,
    },
    img: {
        type: String, 
        required: true,
        validate: {
            validator: imgUrl => isImgUrl(imgUrl),
            message: 'img is invalid url'
        }
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

CategorySchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
CategorySchema.plugin(autoIncrement.plugin, { model: 'category', startAt: 1 });

export default mongoose.model('category', CategorySchema);