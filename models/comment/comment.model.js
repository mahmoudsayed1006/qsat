import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const CommentSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    comment: {
        type: String,
        trim: true,
        required: true,
    },
    rate:{
        type:Number,
        required:true
    },
    user:{
        type:Number,
        required:true,
        ref:'user'
    },
    product:{
        type:Number,
        required:true,
        ref:'product'
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

CommentSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
CommentSchema.plugin(autoIncrement.plugin, { model: 'comment', startAt: 1 });

export default mongoose.model('comment', CommentSchema);