import Branch from "../../models/branch/branch.model";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import { body } from "express-validator/check";
import { checkValidations } from "../shared/shared.controller";
import ApiError from "../../helpers/ApiError";
import { checkExist ,checkExistThenGet} from "../../helpers/CheckMethods";
import ApiResponse from "../../helpers/ApiResponse";

export default {
    validateBody(isUpdate = false) {
        return [
            body('branchName').not().isEmpty().withMessage('branchName Required')
            .custom(async (val, { req }) => {
                let query = {branchName: val, deleted: false };

                if (isUpdate)
                    query._id = { $ne: req.params.branchId };

                let branch = await Branch.findOne(query).lean();
                if (branch)
                    throw new Error('branch duplicated name');

                return true;
            })
            
        ];
    },
    async create(req, res, next) {
        try {
            let user = req.user;
            if (user.type =='CLIENT')
                return next(new ApiError(403, ('admin.auth')));
            const validatedBody = checkValidations(req);
            let branch = await Branch.create({ ...validatedBody });
            let reports = {
                "action":"Create New Branch",
            };
            let report = await Report.create({...reports, user: user });
            return res.status(201).send(branch);
        } catch (error) {
            next(error);
            send(error);
        }
    },
    async getById(req, res, next) {
        try {
            let user = req.user;
            let { branchId } = req.params;

            if (user.type =='CLIENT')
                return next(new ApiError(403, ('admin.auth')));
            await checkExist(branchId, Branch, { deleted: false });

            let branch = await Branch.findById(branchId);
            return res.send(branch);
        } catch (error) {
            next(error);
        }
    },
    async update(req, res, next) {
        try {
            let user = req.user;
            let { branchId } = req.params;

            if (user.type =='CLIENT')  
                return next(new ApiError(403, ('admin.auth')));
            const validatedBody = checkValidations(req);
            let branch = await Branch.findByIdAndUpdate(branchId, { ...validatedBody });

            let reports = {
                "action":"Update Branch",
            };
            let report = await Report.create({...reports, user: user });
            return res.status(200).send(branch);
        } catch (error) {
            next(error);
        }
    },
    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = {deleted: false };
            let Branchs = await Branch.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const branchCount = await Branch.count(query);
            const pageCount = Math.ceil(branchCount / limit);

            res.send(new ApiResponse(Branchs, page, pageCount, limit, branchCount, req));
        } catch (err) {
            next(err);
        }
    },
    async delete(req, res, next) {
        let { branchId } = req.params;
        try {
            let user = req.user;
            if (user.type =='CLIENT')
               return next(new ApiError(403, ('admin.auth')));
            let branch = await checkExistThenGet(branchId, Branch);
            
            branch.deleted = true;
            await branch.save();
            let reports = {
                "action":"Delete branch",
            };
            let report = await Report.create({...reports, user: user });
            res.send('delete success');

        } catch (err) {
            next(err);
        }
    },


}