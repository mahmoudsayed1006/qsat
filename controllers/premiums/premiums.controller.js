import ApiResponse from "../../helpers/ApiResponse";
import ApiError from '../../helpers/ApiError';
import Report from "../../models/reports/report.model";
import Notif from "../../models/notif/notif.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import Premiums from "../../models/Premiums/premiums.model";
import Order from "../../models/order/order.model";
import User from "../../models/user/user.model";

const populateQuery = [
    { path: 'salesMan', model: 'user' },
    { path: 'client', model: 'user' },
    { path: 'branch', model: 'branch' },
    {
        path: 'product', model: 'product',
        populate: { path: 'category', model: 'category' }
    }
];
const populateQuery2 = [
    { path: 'salesMan', model: 'user' },
    { path: 'client', model: 'user' }
];
export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {productId, client,status,salesMan,date,branch} = req.query;
            let query = {deleted: false };
            if (productId) query.product = productId;
            if (client) query.client = client;
            if (salesMan) query.salesMan = salesMan;
            if (status) query.status = status;
            if (branch) query.branch = branch;
            var mydate = new Date(date);
            console.log(mydate);
            var mm = mydate.getMonth() + 1;
            console.log(mm);
            var yyyy = mydate.getFullYear();
            if(mm<10) {
                mm = '0'+mm
            } 
            let from = yyyy + '-' + mm + '-' + '01' + 'T00:00:00.000Z';
            let to= yyyy + '-' + mm + '-' + '30' + 'T23:59:00.000Z';
            if(date) {
                query = { 
                    paidDate: { $gt : new Date(from), $lt : new Date(to) }
               };
            } 
            let premiums = await Premiums.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const premiumsCount = await Premiums.count(query);
            const pageCount = Math.ceil(premiumsCount / limit);

            res.send(new ApiResponse(premiums, page, pageCount, limit, premiumsCount, req));
        } catch (err) {
            next(err);
        }
    },
    validateCreatedPremiums(isUpdate = false) {
        
        let validations = [
            body('status').not().isEmpty().withMessage('status is required'),
            body('client').optional().isNumeric().withMessage('numeric value required'),
            body('product').optional().isNumeric().withMessage('numeric value required'),
            body('salesMan').not().isEmpty().withMessage('salesMan is required')
            .isNumeric().withMessage('numeric value required'),
            body('totalMonth').not().isEmpty().withMessage('totalMonth is required')
            .isNumeric().withMessage('numeric value required'),
            body('costRemaining').not().isEmpty().withMessage('costRemaining is required')
            .isNumeric().withMessage('numeric value required'),
            body('monthRemaining').not().isEmpty().withMessage('monthRemaining is required')
            .isNumeric().withMessage('numeric value required'),
            body('costPerMonth').not().isEmpty().withMessage('costPerMonth is required')
            .isNumeric().withMessage('numeric value required'),
            body('paidMonth').not().isEmpty().withMessage('paidMonth is required')
            .isNumeric().withMessage('numeric value required'),
            body('paidCost').not().isEmpty().withMessage('paidCost is required')
            .isNumeric().withMessage('numeric value required'),
            body('paidDate').not().isEmpty().withMessage('paidDate is required')
            
        ];
    
        return validations;
    },
    async create(req, res, next) {
        try {
           
            const validatedBody = checkValidations(req);
            let premiums = await Premiums.create({
                ...validatedBody,
            });
            let reports = {
                "action":"Create New Premiums",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(201).send(premiums);
            
        } catch (err) {
            next(err);
        }
    },
    async findById(req, res, next) {
        try {
            let { premiumsId } = req.params;
            await checkExist(premiumsId, Premiums, { deleted: false });
            let premiums = await Premiums.findById(premiumsId).populate(populateQuery);
            res.send(premiums);
        } catch (err) {
            next(err);
        }
    },
    async updateSalesMan(req, res, next) {

        try {
            let user = req.user;
            let { premiumsId } = req.params;
            let premiums = await checkExistThenGet(premiumsId, Premiums, { deleted: false });
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            premiums.salesMan = req.body.newSalesMan;
            await premiums.save();
            let salesman = await checkExistThenGet(premiums.salesMan, User, { deleted: false });
console.log(salesman)
            let arr = salesman.clients;
            let old = req.body.oldSalesMan;
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == old){
                    arr.splice(arr[i], 1);
                }
            }
            salesman.clients = arr;
            await salesman.save();
            sendNotifiAndPushNotifi({////////
                targetUser: premiums.client, 
                fromUser: req.user._id, 
                text: 'new notification',
                subject: premiumsId,
                subjectType: 'your salesMan has been changed'
            });
            let notif = {
                "description":'your salesMan has been changed',
                "arabicDescription":'تم تغيير المندوب الخاص بك'
            }
            Notif.create({...notif,resource:req.user._id,target:premiums.client,subject:premiumsId});
            let reports = {
                "action":"Update premiums",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(premiums);
        }
        catch (err) {
            next(err);
        }
    },
    async paid(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'SALES-MAN')
                return next(new ApiError(403, ('SALES-MAN.auth')));
            
            let {premiumsId} = req.params;
            let premiums = await checkExistThenGet(premiumsId, Premiums,
                {deleted: false });
            let newPaidDate = new Date(premiums.paidDate)
            newPaidDate.setMonth( newPaidDate.getMonth() + 1 );
            premiums.monthRemaining = premiums.totalMonth - 1, 
            premiums.costRemaining = premiums.costRemaining - premiums.costPerMonth,
            premiums.paidCost = premiums.paidCost + premiums.costPerMonth,
            premiums.paidMonth = premiums.totalMonth - premiums.monthRemaining, 
            premiums.paidDate = newPaidDate,
            premiums.notes ="",
            await premiums.save();
            let users = await User.find({'type':'ADMIN'});
            users.forEach(user => {
                sendNotifiAndPushNotifi({////////
                    targetUser: user.id, 
                    fromUser: req.user._id, 
                    text: 'new notification',
                    subject: premiumsId,
                    subjectType: 'user did paid the premium '
                });
                let notif = {
                    "description":'user did paid the premium',
                    "arabicDescription":'تم دفع القسط'
                }
                Notif.create({...notif,resource:req.user._id,target:user.id,subject:premiumsId});
            });
            let reports = {
                "action":"user paid premium",
            };
            let report = await Report.create({...reports, user: user });
            res.send(premiums);
            
        } catch (error) {
            next(error);
        }
    },
    async unpaid(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'SALES-MAN')
                return next(new ApiError(403, ('SALES-MAN.auth')));
            
            let {premiumsId} = req.params;
            let premiums = await checkExistThenGet(premiumsId, Premiums,
                {deleted: false });
            let newPaidDate = new Date(premiums.paidDate)
            newPaidDate.setMonth( newPaidDate.getMonth() + 1 );
            premiums.paidDate = newPaidDate,
            premiums.notes = req.body.notes,
            await premiums.save();
            let users = await User.find({'type':'ADMIN'});
            users.forEach(user => {
                sendNotifiAndPushNotifi({////////
                    targetUser: user.id, 
                    fromUser: req.user._id, 
                    text: 'new notification',
                    subject: premiumsId,
                    subjectType: 'user did not paid the premium '
                });
                let notif = {
                    "description":'user did not paid the premium',
                    "arabicDescription":'لم يتم دفع القسط'
                }
                Notif.create({...notif,resource:req.user._id,target:user.id,subject:premiumsId});
            });
            let reports = {
                "action":"user paid premium",
            };
            let report = await Report.create({...reports, user: user });
            res.send(premiums);
            
        } catch (error) {
            next(error);
        }
    },
    async finish(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let {premiumsId} = req.params;
            let premiums = await checkExistThenGet(premiumsId, Premiums,
                {deleted: false });
            premiums.status = "FINISHED";
            await premiums.save();
            let users = await User.find({'type':'ADMIN'});
            users.forEach(user => {
                sendNotifiAndPushNotifi({////////
                    targetUser: user.id, 
                    fromUser: req.user._id, 
                    text: 'new notification',
                    subject: premiumsId,
                    subjectType: 'user finished the premium '
                });
                let notif = {
                    "description":'user finished the premium',
                    "arabicDescription":'تم الانتهاء من القسط'
                }
                Notif.create({...notif,resource:req.user._id,target:user.id,subject:premiumsId});
            });
            let reports = {
                "action":"finish premiums",
            };
            let report = await Report.create({...reports, user: user });
            res.send('premiums finish');
            
        } catch (error) {
            next(error);
        }
    },
    async unfinish(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let {premiumsId} = req.params;
            let premiums = await checkExistThenGet(premiumsId, Premiums,
                {deleted: false });
            premiums.status = "PENDING";
            await premiums.save();
            let users = await User.find({'type':'ADMIN'});
            users.forEach(user => {////
                sendNotifiAndPushNotifi({////////
                    targetUser: user.id, 
                    fromUser: req.user._id, 
                    text: 'new notification',
                    subject: premiumsId,
                    subjectType: 'user did not finish the premium '
                });
                let notif = {
                    "description":'user did not finished the premium '
                }
                Notif.create({...notif,resource:req.user._id,target:user.id,subject:premiumsId});
            });
            let reports = {
                "action":"unfinish premiums",
            };
            let report = await Report.create({...reports, user: user });
            res.send('premiums unfinish');
            
        } catch (error) {
            next(error);
        }
    },
    async update(req, res, next) {
        try {
            
            let {premiumsId } = req.params;
            await checkExist(premiumsId, Premiums,
                {deleted: false });

            const validatedBody = checkValidations(req);
            console.log(validatedBody);
            let premiums = await checkExistThenGet(premiumsId, Premiums,
                {deleted: false });
            let updatedpremiums = await Premiums.findByIdAndUpdate(premiumsId, {
                ...validatedBody,
                costPerMonth:validatedBody.costPerMonth
            }, { new: true }).populate(populateQuery);

            let reports = {
                "action":"Update premiums",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(200).send(updatedpremiums);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let user = req.user;
            let { premiumsId } = req.params;
            let premiums = await checkExistThenGet(premiumsId, Premiums, { deleted: false });
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
            premiums.deleted = true;
            await premiums.save();
            let reports = {
                "action":"Delete premiums",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
    async findAllClients(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {status,salesMan,client} = req.query;
            let query = {deleted: false };
            if (salesMan) query.salesMan = salesMan;
            if (client) query.client = client;
            if (status) query.status = status;
            let premiums = await Premiums.find(query).select('client salesMan').populate(populateQuery2)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const premiumsCount = await Premiums.count(query);
            const pageCount = Math.ceil(premiumsCount / limit);

            res.send(new ApiResponse(premiums, page, pageCount, limit, premiumsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async SendRememberNotif(req, res, next) {
        try {
            let premiums = await Premiums.find({deleted: false ,status:'PENDING'}).select('client');
            console.log(premiums.client);
            premiums.forEach(premium => {
                sendNotifiAndPushNotifi({
                    targetUser: premium.client, 
                    fromUser: req.user._id, 
                    text: 'new notification',
                    subject: premium.id,
                    subjectType: 'Remember to pay the monthly installment'
                });
                let notif = {
                    "description":'Remember to pay the monthly installment',
                    "arabicDescription":'تذكرك لديك قسط شهرى يجب دفعه'
                }
                console.log(premium.client);
                Notif.create({...notif,resource:req.user._id,target:premium.client,subject:premium.id});
            });
            let reports = {
                "action":"send remember notif",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(200).send('notification send');
        } catch (error) {
            next(error)
        }
    },
};