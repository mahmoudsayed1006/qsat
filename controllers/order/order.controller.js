import { checkExist, checkExistThenGet, isLng, isLat, isArray, isNumeric } from "../../helpers/CheckMethods";
import ApiResponse from "../../helpers/ApiResponse";
import Order from "../../models/order/order.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import { body } from "express-validator/check";
import Product from "../../models/product/product.model";
import { ValidationError } from "mongoose";
import { checkValidations } from "../shared/shared.controller";
import ApiError from "../../helpers/ApiError";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import Premiums from "../../models/Premiums/premiums.model";

import Notif from "../../models/notif/notif.model"

const populateQuery = [
    { path: 'client', model: 'user' },
    { path: 'salesMan', model: 'user' },
    {
        path: 'productOrders.product', model: 'product',
        populate: { path: 'category', model: 'category' }
    }
];
function validatedestination(location) {
    if (!isLng(location[0]))
        throw new ValidationError.UnprocessableEntity({ keyword: 'location', message: 'location[0] is invalid lng' });
    if (!isLat(location[1]))
        throw new ValidationError.UnprocessableEntity({ keyword: 'location', message: 'location[1] is invalid lat' });
}
function checkMyAuthorityToUpdateTheOrder(currentUser, statusInBody, order) {
    switch (order.status) {
        case 'PENDING':
            if (statusInBody !== 'ACCEPTED' && statusInBody !== 'REFUSED')
                throw new ApiError(400, 'status should be "ACCEPTED" OR "REFUSED"');
            if (currentUser.type != 'ADMIN')
                throw new ApiError.Forbidden('Order\'s admin is the only one who can accept, refuse or finish the order');
            break;
        case 'ACCEPTED':
            if (statusInBody !== 'ON_THE_WAY')
                throw new ApiError(400, 'status should be "ON_THE_WAY"');
            if (currentUser.type != 'ADMIN')
                throw new ApiError.Forbidden('Order\'s admin is the only one who can accept, refuse or finish the order');
            break;
        case 'ON_THE_WAY':
            if (statusInBody !== 'DELIVERED')
                throw new ApiError(400, 'status should be "DELIVERED"');
            // check for client only in case it's DELIVERY_GUY so it will be set by deliveryGuyOrder status ontheway
            if (currentUser.type != 'ADMIN')
                throw new ApiError.Forbidden('Order\'s admin is the only one who can confirm delivered the order');
            break;
        case 'REFUSED':
        case 'DELIVERED':
            throw new ApiError(400, 'you can\'t update order which is REFUSED or DELIVERED');
    }
}
export default {
    async findOrders(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20
                ,{ status,clientId,paymentSystem,monthCount ,accept} = req.query
                , query = {deleted: false };

            if (status) query.status = status;
            if (accept) query.accept = accept;
            if (clientId) query.client = clientId;
            if (paymentSystem) query.paymentSystem = paymentSystem;
            if(monthCount) query.monthCount = monthCount;
            let orders = await Order.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const ordersCount = await Order.count(query);
            const pageCount = Math.ceil(ordersCount / limit);

            res.send(new ApiResponse(orders, page, pageCount, limit, ordersCount, req));
        } catch (err) {
            next(err);
        }
    },
    validateCreatedOrders() {
        let validations = [
            body('destination').not().isEmpty().withMessage('destination is required'),
            body('productOrders').custom(vals => isArray(vals)).withMessage('productOrders should be an array')
                .isLength({ min: 1 }).withMessage('productOrders should have at least one element of productOrder')
                .custom(async (productOrders, { req }) => {
                    let prevProductId;
                    for (let productOrder of productOrders) {
                        // check if it's duplicated product
                        if (prevProductId && prevProductId === productOrder.product)
                            throw new Error(`Duplicated Product : ${productOrder.product}`);
                        prevProductId = productOrder.product;
                        
                        // check if count is a valid number 
                        if (!isNumeric(productOrder.count))
                            throw new Error(`Product: ${productOrder.product} has invalid count: ${productOrder.count}!`);
                             // check if mounth count is a valid number 
                        if (!isNumeric(productOrder.monthCount))
                        throw new Error(`Product: ${productOrder.product} has invalid mounth count: ${productOrder.monthCount}!`);

                         // check if mounth count is a valid number 
                         if (!isNumeric(productOrder.firstPaid))
                         throw new Error(`Product: ${productOrder.product} has invalid mounth firstPaid: ${productOrder.firstPaid}!`);

                          // check if mounth count is a valid number 
                        if (!isNumeric(productOrder.costPerMonth))
                        throw new Error(`Product: ${productOrder.product} has invalid costPerMonth: ${productOrder.costPerMonth}!`);
                    }
                    return true;
                }),
        ];
        return validations;
    },
            
    async create(req, res, next) {
        try {
            await checkExist(req.user._id, User);
            let user = req.user;
            if (user.active != true)
            return next(new ApiError(403, ('blocked'))); 
            const validatedBody = checkValidations(req);
            validatedestination(validatedBody.destination);
            validatedBody.destination = { type: 'Point', coordinates: [+req.body.destination[0], +req.body.destination[1]] };
            let total = 0;
            for (let singleProduct of validatedBody.productOrders) {
                let productDetail = await checkExistThenGet(singleProduct.product, Product);
                if(singleProduct.paymentSystem == "cash"){
                    if(productDetail.hasOffer == true){
                        total += productDetail.offer * singleProduct.count;
                    } else{
                        total += productDetail.cashPrice * singleProduct.count;
                    }
                    
                } 
                if(singleProduct.paymentSystem == "installment"){
                   
                    if(productDetail.hasOffer == true){
                        total += productDetail.offer * singleProduct.count;
                    } else{
                        total += productDetail.installmentPrice * singleProduct.count;
                    }
                   
                }
                    
            }
            console.log(total)
            validatedBody.total = total;
            
            let createdOrder = await Order.create({ ...validatedBody,client: req.user});
            let order = await Order.populate(createdOrder, populateQuery);
            let reports = {
                "action":"Create New Order",
            };
            let report = await Report.create({...reports, user: req.user });
            let users = await User.find({'type':'ADMIN'});
            users.forEach(user => {
                sendNotifiAndPushNotifi({////////
                    targetUser: user.id, 
                    fromUser: req.user._id, 
                    text: 'new notification',
                    subject: createdOrder.id,
                    subjectType: 'new order'
                });
                let notif = {
                    "description":'new order',
                    "arabicDescription":'طلب جديد'
                }
                Notif.create({...notif,resource:req.user._id,target:user.id,subject:createdOrder.id});
            });
            sendNotifiAndPushNotifi({
                targetUser: req.user._id, 
                fromUser: 'Qsat', 
                text: 'new notification',
                subject: createdOrder.id,
                subjectType: 'your order on progress'
            });
            let notif = {
                "description":'your order on progress',
                "arabicDescription":'جارى تنفيذ طلبك'
            }
            Notif.create({...notif,resource:req.user,target:req.user._id,subject:createdOrder.id});
            
            res.status(201).send(order);
        } catch (err) {
            next(err);
            
        }
    },

    async findById(req, res, next) {
        try {
            let {orderId } = req.params;
            res.send(
                await checkExistThenGet(orderId, Order, { deleted: false, populate: populateQuery })
            );
        } catch (err) {
            next(err);
        }
    },
    async changeOrderStatus(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth'))); 
            let { userId, orderId } = req.params;
            await (userId, User);
            let order = await checkExistThenGet(orderId, Order, { deleted: false });
            const validatedBody = checkValidations(req);
            let statusInBody = validatedBody.status;

            checkMyAuthorityToUpdateTheOrder(req.user, statusInBody, order);
            await Order.findByIdAndUpdate(orderId, { status: statusInBody });
            if(statusInBody == "ACCEPTED"){
                order.salesMan = validatedBody.salesMan;
                order.accept = true;
                await order.save();
                for (let singleProduct of order.productOrders) {
                    if(singleProduct.paymentSystem == "installment"){
                        let productDetail = await checkExistThenGet(singleProduct.product,Product);
                        let paidMonth = 0;
                        let costRemaining ; 
                        if(productDetail.hasOffer == true){
                            costRemaining = productDetail.installmentPrice - singleProduct.firstPaid; 
                        } else{
                            costRemaining = productDetail.offer - singleProduct.firstPaid; 
                        }
                        console.log(productDetail._id);
                        console.log(validatedBody.paidDate);
                        let premiums = await Premiums.create({
                            //productCount:singleProduct.count,
                            //total:productDetail.installmentPrice * singleProduct.count,
                            client: order.client,
                            product: productDetail._id,
                            salesMan: validatedBody.salesMan,
                            branch: validatedBody.branch,
                            totalMonth: singleProduct.monthCount,
                            monthRemaining: singleProduct.monthCount, 
                            costRemaining: costRemaining,
                            paidCost: singleProduct.firstPaid,
                            firstPaid: singleProduct.firstPaid,
                            paidMonth: paidMonth, 
                            paidDate:validatedBody.paidDate,
                            costPerMonth:singleProduct.costPerMonth
                            
                        });
                        let clients = await checkExistThenGet(validatedBody.salesMan, User);
                        
                        let arr = clients.clients;
                        var found = arr.find(function(element) {
                            return element == order.client;
                        });
                        if(!found){
                            clients.clients.push(order.client);
                            await clients.save();
                        }
                        let reports = {
                            "action":"Create New premium",
                        };
                        let report = await Report.create({...reports, user: req.user });
                    }
                    if(singleProduct.paymentSystem == "cash"){
                        let order = await checkExistThenGet(orderId, Order);
                        order.salesMan = validatedBody.salesMan;
                        await order.save();
                    }
                        
                }
                sendNotifiAndPushNotifi({
                    targetUser: order.client, 
                    fromUser: req.user, 
                    text: 'new notification',
                    subject: order.id,
                    subjectType: 'Qsat accept your order'
                });
                let notif = {
                    "description":'Qsat accept your order',
                    "arabicDescription":'تم قبول طلبك'
                }
                Notif.create({...notif,resource:req.user,target:order.client,subject:order.id});
                
            }
            if(statusInBody == "REFUSED"){
                let order = await checkExistThenGet(orderId, Order);
                order.reason = validatedBody.reason;
                await order.save();
                sendNotifiAndPushNotifi({
                    targetUser: order.client, 
                    fromUser: req.user, 
                    text: 'new notification',
                    subject: order.id,
                    subjectType: 'Qsat refused your order'
                });
                let notif = {
                    "description":'Qsat refused your order',
                    "arabicDescription":'تم رفض طلبك'
                }
                Notif.create({...notif,resource:req.user,target:order.client,subject:order.id});
            }

            let reports = {
                "action":"Change Order Status",
            };
            let report = await Report.create({...reports, user: req.user,actionOn:orderId  });
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    },

    validateThenUpdateOrderStatus() {
        let validations = [
            body('status').not().isEmpty().withMessage('status is required')
                .isIn(['ACCEPTED', 'REFUSED', 'ON_THE_WAY', 'DELIVERED']).withMessage('status should be one of ACCEPTED, REFUSED, FINISHED, DELIVERED')
        ,body('paidDate'),
        body('salesMan'),
        body('branch'),
        body('reason')
        ];
        return [validations, this.changeOrderStatus];
    },
    async accept(req, res, next) {
        try {
            let { orderId } = req.params;
            await checkExist(orderId, Order,
                {deleted: false });
            let order = await checkExistThenGet(orderId, Order, { deleted: false });
            for (let singleProduct of order.productOrders) {
                if(singleProduct.paymentSystem == "installment"){
                    singleProduct.firstPaid =req.body.firstPaid;
                    singleProduct.monthCount =req.body.monthCount;
                    singleProduct.costPerMonth =req.body.costPerMonth;
                }
            }
            order.status = 'PENDING'
            await order.save();
            let users = await User.find({'type':'ADMIN'});
            users.forEach(user => {
                sendNotifiAndPushNotifi({////////
                    targetUser: user.id, 
                    fromUser: req.user._id, 
                    text: 'new notification',
                    subject: orderId,
                    subjectType: 'user accept order update'
                });
                let notif = {
                    "description":'user accept order update',
                    "arabicDescription":'تم قبول التعديلات على الطلب'
                }
                Notif.create({...notif,resource:req.user._id,target:user.id,subject:orderId});
            });
            let reports = {
                "action":"user accept order update",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(200).send(order);
        } catch (error) {
            next(error)
        }
    },
    
    async delete(req, res, next) {
        try {
            let { orderId } = req.params;
            let order = await checkExistThenGet(orderId, Order);
            if (['ACCEPTED', 'REFUSED', 'ON_THE_WAY', 'DELIVERED'].includes(order.status))
                next(ApiError(400, 'status should be "PENDING" to delete this order'));
            order.deleted = true;
            await order.save();
            let reports = {
                "action":"Delete Order",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
}