import Product from "../../models/product/product.model";
import ApiResponse from "../../helpers/ApiResponse";
import Category from "../../models/category/category.model";
import { handleImgs, checkValidations } from "../shared/shared.controller";
import { checkExistThenGet, checkExist, isArray } from "../../helpers/CheckMethods";
import { body } from "express-validator/check";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import { toImgUrl } from "../../utils";

import Notif from "../../models/notif/notif.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import ApiError from '../../helpers/ApiError';
import Comments from "../../models/comment/comment.model";
import Favourite from "../../models/favourite/favourite.model";

const populateQuery = [
    { path: 'category', model: 'category' },
    { path: 'comments', model: 'comment' }
];
export default {
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {categoryId, name ,visible,salesRatio,top,company,sort,hasOffer} = req.query;
            let query = {deleted: false };
            if (categoryId) query.category = categoryId;
            if (name) query.name = name;
            if (salesRatio) query.salesRatio = salesRatio;
            if (visible) query.visible = visible;
            if (top) query.top = top;
            if (company) query.company = company;
            if (hasOffer) query.hasOffer = hasOffer;
            let products;
            if(sort == "up"){
                products = await Product.find(query).populate(populateQuery)
                .sort({ installmentPrice: -1 })
                .limit(limit)
                .skip((page - 1) * limit);
           }
           if(sort == "down"){
                products = await Product.find(query).populate(populateQuery)
               .sort({ installmentPrice: 1 })
               .limit(limit)
               .skip((page - 1) * limit);
           }
           if(sort == undefined){
                 products = await Product.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);
           }
            const productsCount = await Product.count(query);
            const pageCount = Math.ceil(productsCount / limit);
            res.send(new ApiResponse(products, page, pageCount, limit, productsCount, req));
        } catch (err) {
            next(err);
        }
    },
    validateCreatedProduct(isUpdate = false) {
        
        let validations = [
            body('name').not().isEmpty().withMessage('name is required'),
            body('company').not().isEmpty().withMessage('company is required'),
            body('description').not().isEmpty().withMessage('description is required'),

            body('cashPrice').not().isEmpty().withMessage('cash Price is required')
            .isNumeric().withMessage('numeric value required'),

            body('installmentPrice').not().isEmpty().withMessage('installment Price is required')
            .isNumeric().withMessage('numeric value required'),

            body('quantity').not().isEmpty().withMessage('quantity is required'),
            body('properties').optional().custom(vals => isArray(vals)).withMessage('properties should be an array')
                .isLength({ min: 1 }).withMessage('properties should have at least one element of properties')
                .custom(async (properties, { req }) => {
                    for (let prop of properties) {
                        body('attr').not().isEmpty().withMessage('attr is required'),
                        body('value').not().isEmpty().withMessage('value is required')
                    }
                    return true;
                }),
            body('category').not().isEmpty().withMessage('category is required')
            .isNumeric().withMessage('numeric value required'),
            
            
        ];
    
        return validations;
    },
    async create(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
            return next(new ApiError(403, ('admin.auth'))); 
            const validatedBody = checkValidations(req);
            await checkExist(validatedBody.category, Category, { deleted: false });
        
            let img = await handleImgs(req);
            console.log(validatedBody.properties)
            let product = await Product.create({
                ...validatedBody,
                img: img, 
            });
            let reports = {
                "action":"Create New Product",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(201).send(product);
            
        } catch (err) {
            next(err);
        }
    },
    async findById(req, res, next) {
        try {
            let { productId } = req.params;

            await checkExist(productId, Product,
                { deleted: false });
            let product = await Product.findById(productId).populate(populateQuery);
            res.send(
                product
            );
        } catch (err) {
            next(err);
        }
    },

    async top(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let {productId} = req.params;
            let product = await checkExistThenGet(productId, Product,
                {deleted: false });
            product.top = true;
            await product.save();
            let reports = {
                "action":"Top Product",
            };
            let report = await Report.create({...reports, user: user });
        
            res.send('product become top');
            
        } catch (error) {
            next(error);
        }
    },

    async low(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth'))); 

            let {productId } = req.params;
            let product = await checkExistThenGet(productId, Product,
                {deleted: false });

            product.top = false;
            await product.save();
            let reports = {
                "action":"Low Product",
            };
            let report = await Report.create({...reports, user: user });
            res.send('product low');
        } catch (error) {
            next(error);
        }
    },
    async active(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let {productId} = req.params;
            let product = await checkExistThenGet(productId, Product,
                {deleted: false });
            product.visible = true;
            await product.save();
            let reports = {
                "action":"Active Product",
            };
            let report = await Report.create({...reports, user: user });
            res.send('product active');
            
        } catch (error) {
            next(error);
        }
    },

    async disactive(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth'))); 

            let {productId } = req.params;
            let product = await checkExistThenGet(productId, Product,
                {deleted: false });

            product.visible = false;
            await product.save();
            let reports = {
                "action":"Dis-active Product",
            };
            let report = await Report.create({...reports, user: user });
            res.send('product dis-active');
        } catch (error) {
            next(error);
        }
    },
    
    async update(req, res, next) {
        try {
            
            let {productId } = req.params;
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth'))); 
            await checkExist(productId, Product,
                {deleted: false });

            
             const validatedBody = checkValidations(req);
            if (req.files) {
                if (req.files['img']) {
                    let imagesList = [];
                    for (let imges of req.files['img']) {
                        imagesList.push(await toImgUrl(imges))
                    }
                    validatedBody.img = imagesList;
                }
            }

            let updatedProduct = await Product.findByIdAndUpdate(productId, {
                ...validatedBody,

            }, { new: true }).populate(populateQuery);
            
           

            let reports = {
                "action":"Update Product",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(200).send(updatedProduct);
        }
        catch (err) {
            next(err);
        }
    },
    async updateMainImg(req, res, next) {
        try {
            
            let {productId } = req.params;
            let product = await checkExistThenGet(productId, Product, { deleted: false });
            let data = [];
            data = product.img;
            console.log(data);
            let index = req.body.index;
            data.unshift(data.splice(index, 1)[0]);
            product.img = data;
            await product.save();
            res.status(200).send(product);
        }
        catch (err) {
            next(err);
        }
    },
    validateOfferBody() {
        return [
            body('offer').not().isEmpty().withMessage('offer is required')
                .isNumeric().withMessage('numeric value'),
            body('offerDescription').not().isEmpty().withMessage('offer is required')
                
        ];

    },
    async addOffer(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth'))); 
            let {productId } = req.params;
            const validatedBody = checkValidations(req);
            let product = await checkExistThenGet(productId, Product,
                { deleted: false });

            product.hasOffer = true;
            product.offer = validatedBody.offer;
            product.offerDescription = validatedBody.offerDescription;
            await product.save();
            let reports = {
                "action":"add offer",
            };
            let report = await Report.create({...reports, user: req.user });
            res.send();
        } catch (error) {
            next(error);
        }
    },

    async removeOffer(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth'))); 
            let { productId } = req.params;
            let product = await checkExistThenGet(productId, Product,
                { deleted: false });

            product.hasOffer = false;
            product.offer = 0;
            await product.save();
            let reports = {
                "action":"Delete offer",
            };
            let report = await Report.create({...reports, user: req.user });
            res.send();
        } catch (error) {
            next(error);
        }
    },


    async delete(req, res, next) {
        try {
            let {productId } = req.params;

            let product = await checkExistThenGet(productId, Product,
                {deleted: false });
            let comments = await Comments.find({ product: productId });
            if(comments){
            for (let comment of comments ) {
                comment.deleted = true;
                await comment.save();
            }
        }
            let favourites = await Favourite.find({ product: productId });
            if(favourites){
            for (let favourite of favourites ) {
                favourite.deleted = true;
                await favourite.save();
            }
        }
            product.deleted = true
            await product.save();
            let reports = {
                "action":"Delete Product",
            };
            let report = await Report.create({...reports, user: req.user._id });
            res.status(204).send();
        }
        catch (err) {
            next(err);
        }
    }
}