import ApiResponse from "../../helpers/ApiResponse";
import ApiError from '../../helpers/ApiError';
import Report from "../../models/reports/report.model";
import Notif from "../../models/notif/notif.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import Notes from "../../models/notes/notes.model";
import Product from "../../models/product/product.model";
import User from "../../models/user/user.model";

const populateQuery = [
    { path: 'product', model: 'product' },
    { path: 'user', model: 'user' }
];
export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {productId, userId} = req.query;
            let query = {deleted: false };
            if (productId) query.product = productId;
            if (userId) query.user = userId;
            let notes = await Notes.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const notesCount = await Notes.count(query);
            const pageCount = Math.ceil(notesCount / limit);

            res.send(new ApiResponse(notes, page, pageCount, limit, notesCount, req));
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('description').not().isEmpty().withMessage('description is required'),
            body('product')

        ];

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            const validatedBody = checkValidations(req);
            console.log(validatedBody);
            let createdNotes = await Notes.create({ ...validatedBody,user:user});
            
            let reports = {
                "action":"Create Notes",
            };
            let report = await Report.create({...reports, user: user});
            let users = await User.find({'type':'ADMIN'});
            users.forEach(user => {
                sendNotifiAndPushNotifi({////////
                    targetUser: user.id, 
                    fromUser: req.user._id, 
                    text: 'new notification',
                    subject: createdNotes.id,
                    subjectType: 'new notes'
                });
                let notif = {
                    "description":'new notes'
                }
                Notif.create({...notif,resource:req.user._id,target:user.id,subject:createdNotes.id});
            });
            res.status(201).send(createdNotes);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { notesId } = req.params;
            await checkExist(notesId, Notes, { deleted: false });
            let notes = await Notes.findById(notesId).populate(populateQuery);
            res.send(notes);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            let user = req.user;
            let { notesId } = req.params;
            let notes = await checkExistThenGet(notesId, Notes, { deleted: false });
            const validatedBody = checkValidations(req);
            let updatednotes = await Notes.findByIdAndUpdate(notesId, {
                ...validatedBody,
            }, { new: true });
            
            let reports = {
                "action":"Update notes",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatednotes);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { notesId } = req.params;
            let notes = await checkExistThenGet(notesId, Notes, { deleted: false });
            notes.deleted = true;
            await notes.save();
            let reports = {
                "action":"Delete notes",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
};