import ApiError from "../../helpers/ApiError";
import Category from "../../models/category/category.model";
import User from "../../models/user/user.model";
import Order from "../../models/order/order.model";
import Product from "../../models/product/product.model";
import Report from "../../models/reports/report.model";
import Premiums from "../../models/Premiums/premiums.model";
import Partners from "../../models/partners/partners.model";

import Ads from "../../models/ads/ads.model";
import Notes from "../../models/notes/notes.model";

const populateQuery = [
    { path: 'client', model: 'user' },
    {
        path: 'productOrders.product', model: 'product',
        populate: { path: 'category', model: 'category' }
    }
];
const action = [
    { path: 'user', model: 'user' },
]
export default {
    async getLastUser(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let query = {
                $and: [
                    {deleted: false},
                    {type:'CLIENT'}
                ]
            };
         
            let lastUser = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastUser);
        } catch (error) {
            next(error);
        }
    },
    async getLastActions(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let query = {deleted: false};
         
            let lastUser = await Report.find(query).populate(action)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastUser);
        } catch (error) {
            next(error);
        }
    },

    async getLastOrder(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let { status} = req.query
            let query = {deleted: false };
            if (status)
                query.status = status;                
            let lastOrder = await Order.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastOrder);
        } catch (error) {
            next(error);
        }
    },
   
    async count(req,res, next) {
        try {
            let query = { deleted: false };
            const usersCount = await User.count(query);
            const categoryCount = await Category.count(query);
            const PendingOrdersCount = await Order.count({deleted:false,status:'PENDING'});
            const AcceptedOrdersCount = await Order.count({deleted:false,status:'ACCEPTED'});
            const RefusedOrdersCount = await Order.count({deleted:false,status:'REFUSED'});
            const DeliveredOrdersCount = await Order.count({deleted:false,status:'DELIVERED'});
            const OnWayOrdersCount = await Order.count({deleted:false,status:'ON_THE_WAY'});
            const productsCount = await Product.count(query);
            const adsCount = await Ads.count(query);
            const notesCount = await Notes.count(query);
            const premiumsCount = await Premiums.count(query);
            const partnersCount = await Partners.count(query);

            var mydate = new Date().toISOString().slice(0, 10);
            var mm = mydate.slice(5, 7);
            var yyyy = mydate.slice(0, 4);
            let from = yyyy + '-' + mm + '-' + '01' + 'T00:00:00.000Z';
            let to= yyyy + '-' + mm + '-' + '30' + 'T23:59:00.000Z';
            console.log(from);
            console.log(to);
            let  query2 = { 
                 paidDate: { $gt : new Date(from), $lt : new Date(to) }
                   
            };
            let totalPremiums = await Premiums.find(query2).select('costPerMonth');
            
            var sumPremiums = 0;
            for (var i = 0; i < totalPremiums.length; i++) { 
                sumPremiums += totalPremiums[i].costPerMonth
            }
            let total = await Order.find(query).select('total');
            var sum = 0;
            for (var i = 0; i < total.length; i++) { 
              sum += total[i].total
            }
            
            res.status(200).send({
                users:usersCount,
                category:categoryCount,
                pending:PendingOrdersCount,
                accepted:AcceptedOrdersCount,
                refused:RefusedOrdersCount,
                delivered:DeliveredOrdersCount,
                onWay:OnWayOrdersCount,
                products:productsCount,
                ads:adsCount,
                notes:notesCount,
                premiums:premiumsCount,
                partners:partnersCount,
                totalPremiums:sumPremiums,
                totalSales:sum
                
            });
        } catch (err) {
            next(err);
        }
        
    },
    
}