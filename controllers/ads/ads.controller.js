import ApiResponse from "../../helpers/ApiResponse";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import Ads from "../../models/ads/ads.model";

export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {top} = req.query;
            let query = {deleted: false };
            if (top) query.top = top;
            let ads = await Ads.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const adsCount = await Ads.count(query);
            const pageCount = Math.ceil(adsCount / limit);

            res.send(new ApiResponse(ads, page, pageCount, limit, adsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findSelection(req, res, next) {
        try {
            let query = { deleted: false };
            let ads = await Ads.find(query)
                .sort({ createdAt: -1 });
            res.send(ads)
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('description').not().isEmpty().withMessage('description is required'),
        ];
        if (isUpdate)
        validations.push([
            body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
        ]);

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
    
            const validatedBody = checkValidations(req);
            console.log(validatedBody);
            let image = await handleImg(req);
            console.log(image);
             console.log(image);
            let createdAds = await Ads.create({ ...validatedBody,img:image});

            let reports = {
                "action":"Create Ads",
            };
            let report = await Report.create({...reports, user: user });
            res.status(201).send(createdAds);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { adsId } = req.params;
            await checkExist(adsId, Ads, { deleted: false });
            let ads = await Ads.findById(adsId);
            res.send(ads);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { adsId } = req.params;
            await checkExist(adsId, Ads, { deleted: false });

            const validatedBody = checkValidations(req);
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validatedBody.img = image;
            }
            let updatedAds = await Ads.findByIdAndUpdate(adsId, {
                ...validatedBody,
            }, { new: true });
            let reports = {
                "action":"Update Ads",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedAds);
        }
        catch (err) {
            next(err);
        }
    },
    async top(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let {adsId} = req.params;
            let ads = await checkExistThenGet(adsId, Ads,
                {deleted: false });
            ads.top = true;
            await ads.save();
            let reports = {
                "action":"Top ads",
            };
            let report = await Report.create({...reports, user: user });
        
            res.send('ads become top');
            
        } catch (error) {
            next(error);
        }
    },

    async low(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth'))); 

            let {adsId } = req.params;
            let ads = await checkExistThenGet(adsId, Ads,
                {deleted: false });

            ads.top = false;
            await ads.save();
            let reports = {
                "action":"Low ads",
            };
            let report = await Report.create({...reports, user: user });
            res.send('ads low');
        } catch (error) {
            next(error);
        }
    },
    async active(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let {adsId} = req.params;
            let ads = await checkExistThenGet(adsId, Ads,
                {deleted: false });
            ads.visible = true;
            await ads.save();
            let reports = {
                "action":"Active ads",
            };
            let report = await Report.create({...reports, user: user });
            res.send('ads active');
            
        } catch (error) {
            next(error);
        }
    },

    async disactive(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth'))); 

            let {adsId } = req.params;
            let ads = await checkExistThenGet(adsId, Ads,
                {deleted: false });

            ads.visible = false;
            await ads.save();
            let reports = {
                "action":"Dis-active ads",
            };
            let report = await Report.create({...reports, user: user });
            res.send('ads dis-active');
        } catch (error) {
            next(error);
        }
    },
    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { adsId } = req.params;
            let ads = await checkExistThenGet(adsId, Ads, { deleted: false });
            
            ads.deleted = true;
            await ads.save();
            let reports = {
                "action":"Delete Ads",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
};