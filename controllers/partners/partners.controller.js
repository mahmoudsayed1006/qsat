import ApiResponse from "../../helpers/ApiResponse";
import ApiError from '../../helpers/ApiError';
import Report from "../../models/reports/report.model";
import Notif from "../../models/notif/notif.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import Partners from "../../models/partners/partners.model";


export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {name} = req.query;
            let query = {deleted: false };
            if (name) query.name = name;
            let partners = await Partners.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const partnersCount = await Partners.count(query);
            const pageCount = Math.ceil(partnersCount / limit);

            res.send(new ApiResponse(partners, page, pageCount, limit, partnersCount, req));
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('name').not().isEmpty().withMessage('name is required'),
            body('cost').not().isEmpty().withMessage('cost is required')
            .isNumeric().withMessage('numeric value required'),
            body('percentage').not().isEmpty().withMessage('percentage is required')
            .isNumeric().withMessage('numeric value required'),
            body('profit')
        ];

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            const validatedBody = checkValidations(req);

            let createdpartners = await Partners.create({ ...validatedBody});
            
            let reports = {
                "action":"Create partners",
            };
            let report = await Report.create({...reports, user: user});
            res.status(201).send(createdpartners);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { partnersId } = req.params;
            await checkExist(partnersId, Partners, { deleted: false });
            let partners = await Partners.findById(partnersId);
            res.send(partners);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth'))); 
            let { partnerId } = req.params;
            let partners = await checkExistThenGet(partnerId, Partners, { deleted: false });
            const validatedBody = checkValidations(req);
            let updatedpartners = await Partners.findByIdAndUpdate(partnerId, {
                ...validatedBody,
            }, { new: true });
            
            let reports = {
                "action":"Update partners",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedpartners);
        }
        catch (err) {
            next(err);
        }
    },
    async finish(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let {partnerId} = req.params;
            let partner = await checkExistThenGet(partnerId, Partners,
                {deleted: false });
            partner.status = true;
            partner.profit = req.body.profit;
            await partner.save();
            let reports = {
                "action":"finish partner",
            };
            let report = await Report.create({...reports, user: user });
            res.send('finish partner');
            
        } catch (error) {
            next(error);
        }
    },
    async unfinish(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let {partnerId} = req.params;
            let partner = await checkExistThenGet(partnerId, Partners,
                {deleted: false });
            partner.status = true;
            await partner.save();
            let reports = {
                "action":"unfinish partner",
            };
            let report = await Report.create({...reports, user: user });
            res.send('unfinish partner');
            
        } catch (error) {
            next(error);
        }
    },

    
    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { partnerId } = req.params;
            let partners = await checkExistThenGet(partnerId, Partners, { deleted: false });
            partners.deleted = true;
            await partners.save();
            let reports = {
                "action":"Delete partners",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
};