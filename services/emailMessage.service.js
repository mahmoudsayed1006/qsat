import nodemailer from 'nodemailer';
import config  from '../config'
let transporter = nodemailer.createTransport({
    pool: true,
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, 
    auth: {
        user: '',
        pass: ''
    }

});

export function sendEmail(targetMail, text) {

    let mailOptions = {
        from: `${config.App.Name}`,
        to: targetMail,
        subject: `${config.App.Name} replay to your messages`,
        text: text,

    };

    console.log('targetMail', targetMail, 'options', mailOptions);

   

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
             console.log(error);
        }
        console.log('Message sent: %s', info.messageId);
        // Preview only available when sending through an Ethereal account
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
    });


    return true;
}
