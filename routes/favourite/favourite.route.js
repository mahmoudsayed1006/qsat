import express from 'express';
import FavouriteController from '../../controllers/favourite/favourite.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/:productId/products')
    .post(
        requireAuth,
        FavouriteController.create
    );
router.route('/:userId/users')
    .get(FavouriteController.findAll);
    
router.route('/:favouriteId')
    .delete( requireAuth,FavouriteController.delete);







export default router;