import express from 'express';
import PartnersController from '../../controllers/partners/partners.controller';
import { requireAuth } from '../../services/passport';
const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        PartnersController.validateBody(),
        PartnersController.create
    );

router.route('/')
    .get(PartnersController.findAll);

router.route('/:partnerId')
    .get(PartnersController.findById)
    .put(
        requireAuth,
        PartnersController.validateBody(true),
        PartnersController.update
    )
    .delete(requireAuth,PartnersController.delete);

router.route('/:partnerId/finish')
    .put(
        requireAuth,
        PartnersController.finish
    )

router.route('/:partnerId/unfinish')
    .put(
        requireAuth,
        PartnersController.unfinish
    )


export default router;
