import express from 'express';
import AdsController from '../../controllers/ads/ads.controller';
import { requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        multerSaveTo('ads').single('img'),
        AdsController.validateBody(),
        AdsController.create
    )
    .get(AdsController.findAll);
    
router.route('/:adsId')
    .put(
        requireAuth,
        multerSaveTo('ads').single('img'),
        AdsController.validateBody(true),
        AdsController.update
    )
    .get(AdsController.findById)
    .delete( requireAuth,AdsController.delete);

router.route('/:adsId/active')
    .put(
        requireAuth,
        AdsController.active
    )

router.route('/:adsId/dis-active')
    .put(
        requireAuth,
        AdsController.disactive
    )

router.route('/:adsId/top')
    .put(
        requireAuth,
        AdsController.top
    )

router.route('/:adsId/low')
    .put(
        requireAuth,
        AdsController.low
    )





export default router;