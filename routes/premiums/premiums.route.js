import express from 'express';
import PremiumsController from '../../controllers/premiums/premiums.controller';
import { requireAuth } from '../../services/passport';
const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        PremiumsController.validateCreatedPremiums(),
        PremiumsController.create
    );

router.route('/')
    .get(PremiumsController.findAll);
router.route('/AllClients')
    .get(PremiumsController.findAllClients);
    
router.route('/:premiumsId')
    .put(requireAuth,PremiumsController.validateCreatedPremiums(true),PremiumsController.update)
    .get(PremiumsController.findById)
    .delete(requireAuth,PremiumsController.delete);

router.route('/:premiumsId/updateSalesMan')
    .put(
        requireAuth,
        PremiumsController.updateSalesMan
    )

router.route('/:premiumsId/finish')
    .put(
        requireAuth,
        PremiumsController.finish
    )

router.route('/:premiumsId/unfinish')
    .put(
        requireAuth,
        PremiumsController.unfinish
    )
router.route('/:premiumsId/paid')
    .put(
        requireAuth,
        PremiumsController.paid
    )
router.route('/:premiumsId/unpaid')
    .put(
        requireAuth,
        PremiumsController.unpaid
    )
router.route('/SendRememberNotif')
    .post(
        requireAuth,
        PremiumsController.SendRememberNotif
    )

export default router;
