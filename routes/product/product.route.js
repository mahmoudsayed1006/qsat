import express from 'express';
import ProductController from '../../controllers/product/product.controller';
import { multerSaveTo } from '../../services/multer-service';
import { requireAuth } from '../../services/passport';
import { parseStringToArrayOfObjectsMw } from '../../utils';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
  
        multerSaveTo('products').array('img',10),
        parseStringToArrayOfObjectsMw('properties'),
        ProductController.validateCreatedProduct(),
        ProductController.create
    );

router.route('/')
    .get(ProductController.findAll);

router.route('/:productId')
    .get(ProductController.findById)
    .put(
        requireAuth,
        multerSaveTo('products').fields([
            { name: 'img', maxCount: 10, options: false }
        ]),
        parseStringToArrayOfObjectsMw('properties'),
        ProductController.validateCreatedProduct(true),
        ProductController.update
    )
    .delete(requireAuth,ProductController.delete);

router.route('/:productId/mainImg')
    .put(
        requireAuth,
        ProductController.updateMainImg
    )
router.route('/:productId/active')
    .put(
        requireAuth,
        ProductController.active
    )

router.route('/:productId/dis-active')
    .put(
        requireAuth,
        ProductController.disactive
    )

router.route('/:productId/top')
    .put(
        requireAuth,
        ProductController.top
    )

router.route('/:productId/low')
    .put(
        requireAuth,
        ProductController.low
    )

router.route('/:productId/offer')
    .put(
        requireAuth,
        ProductController.validateOfferBody(),
        ProductController.addOffer
    )
    .delete( requireAuth,ProductController.removeOffer);


export default router;
