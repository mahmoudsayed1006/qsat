import express from 'express';
import BranchController from '../../controllers/branch/branch.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        BranchController.validateBody(),
        BranchController.create
    )
 .get(BranchController.findAll);
    
router.route('/:branchId')
    .put(
        requireAuth,
        BranchController.validateBody(true),
        BranchController.update
    )
    .get(BranchController.getById)
    .delete( requireAuth,BranchController.delete);




export default router;