import express from 'express';
import NotesController from '../../controllers/notes/notes.controller';
import { requireAuth } from '../../services/passport';
const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        NotesController.validateBody(),
        NotesController.create
    );

router.route('/')
    .get(NotesController.findAll);

router.route('/:notesId')
    .get(NotesController.findById)
    .put(
        requireAuth,
        NotesController.validateBody(true),
        NotesController.update
    )
    .delete(requireAuth,NotesController.delete);



export default router;
