import express from 'express';
import userRoute from './user/user.route';
import CategoryRoute  from './category/category.route';
import AdsRoute  from './ads/ads.route';
import ProductRoute  from './product/product.route';
import OrderRoute  from './order/order.route';
import CommentRoute  from './comment/comment.route';
import ContactRoute  from './contact/contact.route';
import ReportRoute  from './reports/report.route';
import NotifRoute  from './notif/notif.route';
import PartnersRoute  from './partners/partners.route';
import PremiumsRoute  from './premiums/premiums.route';
import FavouritesRoute  from './favourite/favourite.route';
import NotesRoute  from './notes/notes.route';
import BranchRoute  from './branch/branch.route';

import AdminRoute  from './admin/admin.route';
import messageRoute  from './message/message.route';

import { requireAuth } from '../services/passport';

const router = express.Router();

router.use('/', userRoute);
router.use('/categories',CategoryRoute);
router.use('/ads',AdsRoute);
router.use('/products', ProductRoute);
router.use('/favourites', FavouritesRoute);
router.use('/partners',PartnersRoute);
router.use('/premiums',PremiumsRoute);
router.use('/orders',OrderRoute);
router.use('/comments',CommentRoute);
router.use('/contact-us',ContactRoute);
router.use('/reports',requireAuth, ReportRoute);
router.use('/notif',requireAuth, NotifRoute);
router.use('/notes', NotesRoute);
router.use('/branch', BranchRoute);
router.use('/admin',requireAuth, AdminRoute);
router.use('/messages',messageRoute);

export default router;
