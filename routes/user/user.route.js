import express from 'express';
import { requireSignIn, requireAuth } from '../../services/passport';
import UserController from '../../controllers/user/user.controller';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();


router.post('/signin', requireSignIn, UserController.signIn);

router.route('/signup')
    .post(
        multerSaveTo('users').single('img'),
        UserController.validateUserCreateBody(),
        UserController.signUp
    );
router.route('/find')
    .get(UserController.findAll);

router.route('/:userId/delete')
    .delete(requireAuth,UserController.delete);

router.route('/:userId/active')
    .put(
        requireAuth,
        UserController.active
    );
router.route('/:userId/disactive')
    .put(
        requireAuth,
        UserController.disactive
    );
router.route('/logout')
    .post(
        requireAuth,
        UserController.logout
    );
router.route('/sendnotif')
    .post(
        requireAuth,
        UserController.validateNotif(),
        UserController.SendNotif
    );
router.route('/addToken')
    .post(
        requireAuth,
        UserController.addToken
    );
router.route('/updateToken')
    .put(
        requireAuth,
        UserController.updateToken
    );


router.put('/check-exist-email', UserController.checkExistEmail);

router.put('/check-exist-phone', UserController.checkExistPhone);

router.put('/user/updateInfo',
    requireAuth,
    multerSaveTo('users').single('img'),
    UserController.validateUpdatedBody(),
    UserController.updateInfo);

    
router.put('/user/updatePassword',
    requireAuth,
    UserController.validateUpdatedPassword(),
    UserController.updatePassword);

router.put('/user/:userId/updateInfo',
    requireAuth,
    multerSaveTo('users').single('img'),
    UserController.validateUpdatedUser(),
    UserController.updateUserInfo);

router.post('/forget-password',
    UserController.validateForgetPassword(),
    UserController.forgetPasswordSms);

router.post('/confirm-code',
    UserController.validateConfirmVerifyCode(),
    UserController.resetPasswordConfirmVerifyCode);

router.post('/reset-password',
    UserController.validateResetPassword(),
    UserController.resetPassword);


export default router;
